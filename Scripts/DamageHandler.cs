﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour {

    public int health = 1;
    int correctLayer;

    public float invulnPeriod = 0;
    float invulnTimer = 0;

    float invulnAnimTimer = 0.2f;

    SpriteRenderer spriteRend;

    void Start()
    {
        correctLayer = gameObject.layer;

        spriteRend = GetComponent<SpriteRenderer>();
        if (spriteRend == null)
        {
            spriteRend = GetComponentInChildren<SpriteRenderer>();

            if (spriteRend == null)
                Debug.LogError("Object '" + gameObject.name + "' has no sprite renderer");
        }
    }

    void OnTriggerEnter2D()
    {
        Debug.Log("Trigger!");

        health--;
        invulnTimer = invulnPeriod;
        gameObject.layer = 10;
    }

    void Update()
    {
        invulnTimer -= Time.deltaTime;
        if(invulnTimer <= 0)
        {
            spriteRend.enabled = true;
            gameObject.layer = correctLayer;
            invulnAnimTimer = 0.2f;
        }
        else
        {
            if (spriteRend != null)
            {
                if (invulnAnimTimer <= 0)
                {
                    spriteRend.enabled = !spriteRend.enabled;
                    invulnAnimTimer = 0.2f;
                }
                else
                    invulnAnimTimer -= Time.deltaTime;
            }
        }

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
        return;
    }
}
