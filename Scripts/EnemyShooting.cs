﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

    public GameObject bulletPrefab;

    Transform player;

    public float fireDelay = 0.5f;
    float cooldownTimer = 0;

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            // Find the player's ship
            GameObject go = GameObject.Find("PlayerShip");
            if (go != null)
            {
                player = go.transform;
            }
        }

        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer <= 0 && player!=null && Vector3.Distance(transform.position, player.position) < 4)
        {
            Debug.Log("Pew!");
            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * new Vector3(0, 0.5f, 0);

            Instantiate(bulletPrefab, transform.position + offset, transform.rotation);
        }
    }

}
